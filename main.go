package main

import (
	"fmt"
	"math/rand"
	"time"
)

func main() {
	// all int for loops here
	gameLoop := 1
	gameLoopMax := 2
	betLoop := 1
	betLoopMax := 2
	choseLoop := 1
	choseLoopMax := 2
	colorloop := 2
	colorLoopMax := 2
	colorGameB := 2
	colorGameBMax := 2
	colorGameR := 2
	colorGameRMax := 2
	numberLoop := 2
	numberLoopMax := 2
	numberGame := 2
	NumberGameMax := 2
	contLoop := 1
	contLoopMax := 2

	// all variables here
	var bet int
	var number int
	var totalWinning int
	var chose string
	var color string
	var cont string
	bank := 1000

	for gameLoop < gameLoopMax {

		// resets loops
		gameLoop = 1
		betLoop = 1
		choseLoop = 1
		colorGameR = 2
		colorGameB = 2
		contLoop = 1

		if bank >= 100 {

		} else {
			fmt.Println("Get some cash")
			return
		}

		for betLoop < betLoopMax {
			fmt.Println("You have", bank, "in cash")
			fmt.Println("Chose a bet between 100, 300 and 500")
			fmt.Scan(&bet)

			switch {
			case bet == 100 && bank >= 100:
				fmt.Println("You bet", bet)
				betLoop = 2

			case bet == 300 && bank >= 300:
				fmt.Println("You bet", bet)
				betLoop = 2

			case bet == 500 && bank >= 500:
				fmt.Println("You bet", bet)
				betLoop = 2

			default:
				fmt.Println("Invalid input")
			}

		}

		for choseLoop < choseLoopMax {
			fmt.Println("Do you want to play [c]olor or [n]umber")
			fmt.Scan(&chose)

			switch {
			case chose == "c":
				fmt.Println("You chose to play color")
				choseLoop = 2
				colorloop = 1
				numberLoop = 2
				numberGame = 2
			case chose == "n":
				fmt.Println("You chose to play number")
				choseLoop = 2
				colorloop = 2
				numberLoop = 1
				numberGame = 1
			default:
				fmt.Println("Invalid input")
			}
		}

		for colorloop < colorLoopMax {
			fmt.Println("Chose a color between [r]ed and [b]lack")
			fmt.Scan(&color)

			switch {
			case color == "r":
				fmt.Println("You chose to play red")
				colorloop = 2
				colorGameB = 2
				colorGameR = 1
			case color == "b":
				fmt.Println("You chose to play color black")
				colorloop = 2
				colorGameB = 1
				colorGameR = 2
			default:
				fmt.Println("Invalid input")
			}
		}

		for numberLoop < numberLoopMax {
			fmt.Println("Chose a number between 1-36")
			fmt.Scan(&number)

			switch {
			case number >= 1 && number <= 36:
				fmt.Println("You chose to play on", number)
				numberLoop = 2
			default:
				fmt.Println("Invalid input")
			}
		}

		rand.Seed(time.Now().UnixNano())
		randomNumber := rand.Intn(36-1) + 1

		for numberGame < NumberGameMax {
			fmt.Println("The number is", randomNumber)

			switch {
			case randomNumber == number:
				fmt.Println("You won", bet+bet)
				bank = bank + bet + bet
				totalWinning = totalWinning + bet + bet
				numberGame = 2
			default:
				fmt.Println("You lose", bet)
				bank = bank - bet
				totalWinning = totalWinning - bet
				numberGame = 2
			}
		}

		for colorGameB < colorGameBMax {
			fmt.Println("The number is", randomNumber)

			switch {
			case randomNumber >= 1 && randomNumber <= 18:
				fmt.Println("You won", bet)
				bank = bank + bet
				totalWinning = totalWinning + bet
				colorGameB = 2
			default:
				fmt.Println("You lose", bet)
				bank = bank - bet
				totalWinning = totalWinning - bet
				colorGameB = 2
			}
		}

		for colorGameR < colorGameRMax {
			fmt.Println("The number is", randomNumber)

			switch {
			case randomNumber >= 19 && randomNumber <= 36:
				fmt.Println("You won", bet)
				bank = bank + bet
				totalWinning = totalWinning + bet
				colorGameR = 2
			default:
				fmt.Println("You lose", bet)
				bank = bank - bet
				totalWinning = totalWinning - bet
				colorGameR = 2
			}
		}

		for contLoop < contLoopMax {
			fmt.Println("you're total winnings are", totalWinning)
			fmt.Println("Continue? y/n")
			fmt.Scan(&cont)

			switch {
			case cont == "y":
				contLoop = 2
				gameLoop = 1
			case cont == "n":
				contLoop = 2
				gameLoop = 2
			default:
				fmt.Println("Invalid input")
			}
		}

	}
	fmt.Println("end of game")
}
